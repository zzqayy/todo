#include "tasklineedit.h"

TaskLineEdit::TaskLineEdit(QWidget *parent) : QLineEdit(parent)
{

}

TaskLineEdit::~TaskLineEdit()
{

}

void TaskLineEdit::mousePressEvent(QMouseEvent *event){
    if (event -> button() == Qt::LeftButton){
        emit clicked();
    }
}
