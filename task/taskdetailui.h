#ifndef TASKDETAILUI_H
#define TASKDETAILUI_H

#include <QFrame>
#include <qcheckbox.h>
#include <qdatetimeedit.h>
#include <qgridlayout.h>
#include <qlineedit.h>
#include <qpushbutton.h>
#include <qabstractitemmodel.h>
#include <qlabel.h>
#include <qcalendarwidget.h>
#include "utils/dbutil.h"
#include "utils/flatui.h"

#include <qmessagebox.h>

class TaskDetailUI : public QFrame
{
    Q_OBJECT
public:
    TaskDetailUI(QFrame *parent = 0);
    ~TaskDetailUI();
private:
    int taskId = 0;
    QDateTime currentDoneTime;
    QGridLayout *mainLayout = new QGridLayout(this);
    QLineEdit *txtEdit = new QLineEdit(this);
    QCalendarWidget *calWidget = new QCalendarWidget(this);
    QLabel *planTimeLabel = new QLabel("计划时间:", this);
    QDateEdit *planTimeEdit = new QDateEdit();
    QCheckBox *enableBox = new QCheckBox("完成", this);
    QCheckBox *importantBox = new QCheckBox("重要", this);
    QCheckBox *collectionBox = new QCheckBox("收藏", this);
    QLabel *createTimeLabel = new QLabel(this);
    QPushButton *subBtn = new QPushButton("提交", this);
    QPushButton *delBtn = new QPushButton("删除", this);
public slots:
     void loadData(const QModelIndex &index);
private slots:
     void subData();
     void delData();
signals:
     void changeData(int taskId);
};

#endif // TASKDETAILUI_H
