#ifndef TASKUI_H
#define TASKUI_H

#include <QFrame>
#include "taskdetailui.h"
#include <qpalette.h>
#include "addtaskview.h"
#include "taskitemdelegate.h"
#include <qsqltablemodel.h>
#include <qtableview.h>
#include <qheaderview.h>
#include "utils/dbutil.h"
#include "utils/flatui.h"
#include "group/groupui.h"
#include "QMenu"
#include "group/ShowGroupView.h"

class TaskUI : public QFrame
{
    Q_OBJECT
public:
    explicit TaskUI(QFrame *parent = 0);
    ~TaskUI();
    enum TaskColum: int {
        Id = 0,
        TaskText = 1,
        GroupId = 2,
        CreateTime = 3,
        PlanTime = 4,
        DoneTime = 5,
        EnableStatus = 6,
        ImportantStatus = 7,
        CollectionStatus = 8
    };
    enum EnableStatus: int {
        Enable = 1,
        Disable = 0
    };
private:
    int typeId;
    int groupId;
    int rightClickTaskId = 0;
    int enableStatus = 0;
    bool isAddShow = false;
    QTableView *taskView = new QTableView(this);
    QSqlTableModel *taskTableModel;
    TaskItemDelegate *taskItemDelegate;
    TaskDetailUI *taskDetailUI = new TaskDetailUI();
    QPushButton *allTaskBtn = new QPushButton("全");
    QPushButton *inCompleteBtn = new QPushButton("未");
    QPushButton *completeBtn = new QPushButton("完");
    QPushButton *addTaskBtn = new QPushButton("+", this);
    AddTaskView *addTaskView = new AddTaskView(this);

    QVBoxLayout *taskLayout = new QVBoxLayout(this);
    QHBoxLayout *statusLayout = new QHBoxLayout();

    ShowGroupView *showGroupView = new ShowGroupView();

    QMenu *doubleClickMenu = new QMenu(this);
    QAction *changeTaskGroupAction = new QAction(tr("更改分组"));
    QAction *editTaskAction = new QAction(tr("编辑"));
    QAction *delTaskAction = new QAction(tr("删除"));

    void init();
    void reloadTaskList(int type, int groupId);
    bool eventFilter(QObject *obj, QEvent *event);
public slots:
    void reloadTasks(int typeId, int groupId);

    void delByGroupId(int delGroupId);

    void chooseTask(const QModelIndex &index);

    void slotContextMenu(QPoint pos);
    void showChangeTaskView();

private slots:
    void showAddTaskUI();
    void goToTaskDetail(const QModelIndex &index);
    void goTaskDetail();
    void reloadTasksWithNo(int taskId);
    void checkedAll();
    void checkedInComplete();
    void checkedComplete();
signals:

    void itemChecked(const QModelIndex &index);
    void clickTask(int taskId);
};

#endif // TASKUI_H
