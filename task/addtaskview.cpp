#include "addtaskview.h"

#include <qpainter.h>
#include <qstyleoption.h>

AddTaskView::AddTaskView(QFrame *parent): QFrame(parent)
{
    this->setStyleSheet(QString("AddTaskView{border-style:none;border: 1px solid gray;border-radius:3px;padding:0px}"));
    this->init();
}

AddTaskView::~AddTaskView()
{
}

void AddTaskView::init()
{
    //布局设置
    this->contextLayout->addWidget(this->taskTxtEdit, 0);
    this->contextLayout->addWidget(this->taskDate, 0, Qt::AlignRight);
    this->contextLayout->addWidget(this->importantBox,0 , Qt::AlignLeft);
    this->setLayout(this->contextLayout);

    //任务内容
    this->taskTxtEdit->setPlaceholderText("任务详情");
    FlatUI::setDefaultLineEditQss(this->taskTxtEdit);
    FlatUI::setAddTaskViewEditQss(this->taskTxtEdit);
    //时间
    QCalendarWidget *calWidget = new QCalendarWidget(this);
    calWidget->setMinimumDate(QDate::currentDate().addMonths(-1));
    calWidget->show();
    FlatUI::setDateEditQss(taskDate, 5, 0);
    this->taskDate->setCalendarPopup(true);  //调用日历控件
    this->taskDate->setCalendarWidget(calWidget);
    this->taskDate->setDate(QDate::currentDate());
    this->taskDate->setDisplayFormat("yyyy/MM/dd");
    this->taskDate->setDateRange(calWidget->minimumDate(), calWidget->maximumDate());
    //重要
    FlatUI::setCheckBox(this->importantBox);

    //添加任务回车事件
    connect(taskTxtEdit, &TaskLineEdit::returnPressed, this, &AddTaskView::addTask);
}


void AddTaskView::addTask()
{
    QString taskTxt = this->taskTxtEdit->text();
    if(taskTxt.isNull() || taskTxt.isEmpty()) {
        QMessageBox::warning(this, "提示", "任务内容不能为空");
        return;
    }
    QDate taskDate = this->taskDate->date();
    if(taskDate.isNull()) {
        QMessageBox::warning(this, "提示", "截至时间不能为空");
        return;
    }
    int importantStatus = 0;
    if(this->importantBox->isChecked()) {
        importantStatus = 1;
    }
    bool addStatus = dbUtil::addTask(taskTxt, groupId, taskDate, importantStatus);
    if (addStatus)
    {
        this->taskTxtEdit->setText("");
        this->taskDate->setDate(QDate::currentDate());
        this->importantBox->setChecked(false);
        emit addVal(this->typeId, this->groupId);
        QMessageBox::warning(this, "提示", "添加成功!");
        return;
    }
}

void AddTaskView::setTypeIdAndGroupId(int typeId, int groupId)
{
    this->typeId = typeId;
    this->groupId = groupId;
}

