#include "taskitemdelegate.h"
#include "taskui.h"

#include <qdatetime.h>
#include <qdebug.h>
#include <qapplication.h>
#include <qevent.h>


TaskItemDelegate::TaskItemDelegate(QObject *parent)
    : QStyledItemDelegate{parent}
{

}

void TaskItemDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    if(index.isValid())
    {
        //获取值
        QVariant variant = index.data(Qt::DisplayRole);
        if(!variant.isValid()) {
            return;
        }
        //列数
        painter->save();
        //用来在视图中画一个item
        QStyleOptionViewItem viewOption(option);

        QRectF rect;
        rect.setX(option.rect.x());
        rect.setY(option.rect.y());
        rect.setWidth(option.rect.width()-1);
        rect.setHeight(option.rect.height()-1);

        //QPainterPath画圆角矩形
        const qreal radius = 5;
        QPainterPath path;
        path.moveTo(rect.topRight() - QPointF(radius, 0));
        path.lineTo(rect.topLeft() + QPointF(radius, 0));
        path.quadTo(rect.topLeft(), rect.topLeft() + QPointF(0, radius));
        path.lineTo(rect.bottomLeft() + QPointF(0, -radius));
        path.quadTo(rect.bottomLeft(), rect.bottomLeft() + QPointF(radius, 0));
        path.lineTo(rect.bottomRight() - QPointF(radius, 0));
        path.quadTo(rect.bottomRight(), rect.bottomRight() + QPointF(0, -radius));
        path.lineTo(rect.topRight() + QPointF(0, radius));
        path.quadTo(rect.topRight(), rect.topRight() + QPointF(-radius, -0));

        //绘制点击颜色
        if(option.state.testFlag(QStyle::State_Selected))
        {
            painter->setPen(QPen(QColor(ConfigUtil::getStyleValue("taskItemClickPenColor"))));
            painter->setBrush(QColor(ConfigUtil::getStyleValue("taskItemClickBrushColor")));
            painter->drawPath(path);
        }
        else{
            painter->setPen(QPen(QColor(ConfigUtil::getStyleValue("taskItemPenColor"))));
            painter->setBrush(Qt::NoBrush);
            painter->drawPath(path);
        }

        int enableStatus = index.model()->index(index.row(), TaskUI::TaskColum::EnableStatus)
                .data().toInt();
        QRect circleRect = QRect(rect.left() + 5,
                                 rect.top()+(rect.height()-12)/2,
                                 12,
                                 12
                                 );
        QPen penBrush;
        if(enableStatus == TaskUI::EnableStatus::Enable)
        {
            //画圆圈
            QColor circleColor = QColor(ConfigUtil::getStyleValue("taskItemCircleClickBrushColor"));
            QBrush brush(circleColor);
            painter->setBrush(circleColor);
            penBrush = QPen(circleColor);
        }
        else
        {
            penBrush = QPen(QColor("taskItemCircleBrushColor"));
        }

        painter->setPen(penBrush);
        painter->drawEllipse(circleRect);

        //绘制开关位置

//        QStyleOptionButton checkboxOpt;
//        if(enableStatus == 1)
//        {
//            checkboxOpt.state |= QStyle::State_On;
//        }else
//        {
//            checkboxOpt.state |= QStyle::State_Off;
//        }
//        checkboxOpt.state |= QStyle::State_Enabled;
//        checkboxOpt.iconSize = QSize(20, 20);
//        checkboxOpt.rect = circleRect;

//        QApplication::style()->drawControl(QStyle::CE_CheckBox, &checkboxOpt, painter);

        //绘制名字
        QString taskTxt = variant.toString();
        QFont lightFont;
        QPen textPen;
        if(enableStatus == 1)
        {
            textPen = QPen(QColor(ConfigUtil::getStyleValue("taskItemNameClickColor")));
            lightFont = QFont(ConfigUtil::getDefaultFont()
                    , 12
                    , QFont::Weight::ExtraLight);
            lightFont.setStrikeOut(true);
        }else
        {
            textPen = QPen(QColor(ConfigUtil::getStyleValue("taskItemNameColor")));
            lightFont = QFont(ConfigUtil::getDefaultFont(), 12);
        }
        painter->setPen(textPen);
        painter->setFont(lightFont);
        QFontMetrics nameMetrics = painter->fontMetrics();
        int nameHeight = nameMetrics.height();
        QRect nameRect = QRect(
                    circleRect.right() + 5
                    , rect.top()+(rect.height()-nameHeight)/2
                    , rect.width()
                    , nameHeight
        );
        painter->drawText(nameRect,Qt::AlignVCenter, taskTxt);

        //绘制时间
        QDate currentDate = index.model()->index(index.row(), TaskUI::TaskColum::PlanTime)
                .data().toDate();
        QString currentDateTxt = currentDate.toString("yyyy/MM/dd");
        painter->setPen(QPen(QColor(ConfigUtil::getStyleValue("taskItemTimeColor"))));
        painter->setFont(QFont(ConfigUtil::getDefaultFont(), 10, QFont::Weight::Thin));
        QFontMetrics dateFontMetrics = painter->fontMetrics();
        int dateMaxWidth = dateFontMetrics.maxWidth() * 10;
        int dateHeight = dateFontMetrics.height();
        QRect dateRect = QRect(rect.right() - dateMaxWidth, nameRect.top(), dateMaxWidth, dateHeight);
        painter->drawText(dateRect,Qt::AlignBottom | Qt::AlignRight, currentDateTxt);

    }
}

QSize TaskItemDelegate::sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    return QSize(100, 100);
}

bool TaskItemDelegate::statusCheckEvent(QEvent *event
                                        , QAbstractItemModel *model
                                        , const QStyleOptionViewItem &option
                                        , const QModelIndex &index
                                        )
{
    QRect docrationRec = option.rect;
    QMouseEvent *mouseEvent = static_cast<QMouseEvent *>(event);
    if (event->type() == QEvent::MouseButtonPress
            && docrationRec.contains(mouseEvent->pos())
            )
        {
            qDebug() << "任务点击事件";
        }
        return true;
}
