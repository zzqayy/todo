#include "taskdetailui.h"


TaskDetailUI::TaskDetailUI(QFrame *parent): QFrame(parent)
{
    this->setStyleSheet(QString("TaskDetailUI{border: 1px solid gray;border-radius:3px}"));
    //返回键
    FlatUI::setCheckBox(this->enableBox);
    this->mainLayout->addWidget(this->enableBox, 0, 11, 1, 2, Qt::AlignRight);

    this->mainLayout->addWidget(this->planTimeLabel, 0, 0, 1, 1);
    this->calWidget->show();
    this->planTimeEdit->setCalendarPopup(true);
    FlatUI::setDateEditQss(this->planTimeEdit);
    this->planTimeEdit->setCalendarWidget(this->calWidget);
    this->planTimeEdit->setDate(QDate::currentDate());
    this->planTimeEdit->setDisplayFormat("yyyy/MM/dd");
    this->mainLayout->addWidget(this->planTimeEdit, 0, 1, 1, 2, Qt::AlignLeft);

    //第0行,第0列,占1行4列
    this->txtEdit->setPlaceholderText("任务详情");
    FlatUI::setLineEditQss(this->txtEdit);
    this->mainLayout->addWidget(this->txtEdit, 1, 0, 1, 12);

    FlatUI::setCheckBox(this->importantBox);
    this->mainLayout->addWidget(this->importantBox, 2, 0, 1, 2);
    FlatUI::setCheckBox(this->collectionBox);
    this->mainLayout->addWidget(this->collectionBox, 2, 2, 1, 2);
    this->mainLayout->addWidget(this->createTimeLabel, 3, 0, 1, 2);

    FlatUI::setNormalBtnColor(this->subBtn);
    this->mainLayout->addWidget(this->subBtn, 3, 10, 1, 1);
     FlatUI::setDelBtnColor(this->delBtn);
    this->mainLayout->addWidget(this->delBtn, 3, 11, 1, 1);

    connect(this->subBtn, &QPushButton::clicked, this, &TaskDetailUI::subData);
    connect(this->delBtn, &QPushButton::clicked, this, &TaskDetailUI::delData);
}
TaskDetailUI::~TaskDetailUI()
{
}

void TaskDetailUI::loadData(const QModelIndex &index)
{
    int dataRow = index.row();
    int currentTaskId = index.model()->index(dataRow, 0)
            .data()
            .toInt();
    this->taskId = currentTaskId;
    QString currentTaskTxt = index.model()->index(dataRow, 1)
            .data()
            .toString();
    this->txtEdit->setText(currentTaskTxt);

    QDate currentPlanTime = index.model()->index(dataRow, 4)
                .data()
                .toDate();
    this->planTimeEdit->setDate(currentPlanTime);

    this->currentDoneTime = index.model()->index(dataRow, 5)
                .data()
                .toDateTime();

    int enableStatus = index.model()->index(dataRow, 6)
                    .data().toInt();
    if(enableStatus == 1)
    {
        this->enableBox->setChecked(true);
    }else
    {
        this->enableBox->setChecked(false);
    }

    int importantStatus = index.model()->index(dataRow, 7)
                    .data().toInt();
    if(importantStatus == 1)
    {
        this->importantBox->setChecked(true);
    }else
    {
        this->importantBox->setChecked(false);
    }

    int collectionStatus = index.model()->index(dataRow, 8)
                    .data().toInt();
    if(collectionStatus == 1)
    {
        this->collectionBox->setChecked(true);
    }else
    {
        this->collectionBox->setChecked(false);
    }

    QDateTime currentCreateTime = index.model()->index(dataRow, 3)
                .data()
                .toDateTime();
    this->createTimeLabel->setText(currentCreateTime.toString("yyyy年MM月dd日 HH时mm分"));
}

void TaskDetailUI::subData()
{
    int enableStatus = 0;
    if(this->enableBox->isChecked())
    {
        enableStatus = 1;
    }
    int importantStatus = 0;
    if(this->importantBox->isChecked())
    {
        importantStatus = 1;
    }
    int collectionStatus = 0;
    if(this->collectionBox->isChecked())
    {
        collectionStatus = 1;
    }
    if(this->currentDoneTime.isNull() && enableStatus == 1)
    {
        this->currentDoneTime = QDateTime::currentDateTime();
    }
    bool taskStatus = dbUtil::updateTask(this->taskId
                       , this->txtEdit->text()
                       , this->planTimeEdit->date()
                       , this->currentDoneTime
                       , enableStatus
                       , importantStatus
                       , collectionStatus
                       );
    if(taskStatus) {
        emit changeData(this->taskId);
        this->close();
    }else {
        QMessageBox::warning(NULL, "消息提示","操作失败!");
    }
}

void TaskDetailUI::delData()
{
    bool taskStatus = dbUtil::delTaskByPrimaryId(this->taskId);
    if(taskStatus) {
        emit changeData(this->taskId);
        this->close();
    }else {
       QMessageBox::warning(NULL, "消息提示","操作失败!");
    }
}
