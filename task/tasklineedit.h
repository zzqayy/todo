#ifndef TASKLINEEDIT_H
#define TASKLINEEDIT_H

#include <QWidget>
#include <QMouseEvent>
#include <QLineEdit>

class TaskLineEdit : public QLineEdit
{
    Q_OBJECT;
public:
    explicit TaskLineEdit(QWidget *parent = 0);
    ~TaskLineEdit();
    void mousePressEvent(QMouseEvent *event);
signals:
    void clicked();
};

#endif // TASKLINEEDIT_H
