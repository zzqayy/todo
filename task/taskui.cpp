#include "taskui.h"

TaskUI::TaskUI(QFrame *parent): QFrame(parent)
{
    this->init();
}

TaskUI::~TaskUI()
{

}

void TaskUI::init()
{
    this->statusLayout->addStretch(8);
    this->statusLayout->addWidget(this->inCompleteBtn, 1);
    this->statusLayout->addWidget(this->completeBtn, 1);
    this->statusLayout->addWidget(this->allTaskBtn, 1);
    FlatUI::setTaskStatusClickBtn(this->inCompleteBtn);
    FlatUI::setTaskStatusNormalBtn(this->completeBtn);
    FlatUI::setTaskStatusNormalBtn(this->allTaskBtn);
    this->taskLayout->addLayout(this->statusLayout, 1);
    //其他两个
    this->taskView->setStyleSheet("QTableView{border: 1px solid gray;border-radius:3px}");
    this->taskLayout->addWidget(this->taskView, 10);

    this->taskItemDelegate = new TaskItemDelegate(this->taskView);
    this->taskView->setContextMenuPolicy(Qt::CustomContextMenu);
    //定义详情
    this->taskDetailUI->setFixedHeight(200);
    this->taskDetailUI->setFixedWidth(400);
    this->addTaskView->hide();

    //初始化变量
    this->taskTableModel = new QSqlTableModel(this->taskView , dbUtil::getDbConnect("task_connect"));

    //默认加载今天的任务
    reloadTasks(GroupUI::Today, 0);

    //添加任务时候触发刷新事件
    connect(this->addTaskBtn, &QPushButton::clicked, this, &TaskUI::showAddTaskUI);
    connect(this->taskView, &QTableView::doubleClicked, this, &TaskUI::chooseTask);
    connect(this->addTaskView, &AddTaskView::addVal, this, &TaskUI::reloadTasksWithNo);

    connect(this, &TaskUI::itemChecked, this->taskDetailUI, &TaskDetailUI::loadData);
    //点击前往任务详情
//    connect(this, &TaskUI::itemChecked, this, &TaskUI::goToTaskDetail);
    connect(this->taskDetailUI, &TaskDetailUI::changeData, this, &TaskUI::reloadTasksWithNo);
    //点击类型显示
    connect(this->inCompleteBtn, &QPushButton::clicked, this, &TaskUI::checkedInComplete);
    connect(this->completeBtn, &QPushButton::clicked, this, &TaskUI::checkedComplete);
    connect(this->allTaskBtn, &QPushButton::clicked, this, &TaskUI::checkedAll);

    //taskView右键
    this->changeTaskGroupAction->setParent(this->doubleClickMenu);
    this->editTaskAction->setParent(this->doubleClickMenu);
    this->delTaskAction->setParent(this->doubleClickMenu);
    this->doubleClickMenu->addAction(this->editTaskAction);
    this->doubleClickMenu->addAction(this->changeTaskGroupAction);
    this->doubleClickMenu->addAction(this->delTaskAction);
    //监听编辑
    connect(this->editTaskAction, &QAction::triggered, this, &TaskUI::goTaskDetail);
    //监听删除
    connect(this->delTaskAction, &QAction::triggered, this, &TaskUI::goTaskDetail);
    //监听事件
    connect(this->taskView, &TaskUI::customContextMenuRequested, this, &TaskUI::slotContextMenu);
    connect(this, &TaskUI::clickTask, this->showGroupView, &ShowGroupView::setChooseTaskId);
    //更改分组
    connect(changeTaskGroupAction, &QAction::triggered, this, &TaskUI::showChangeTaskView);
    connect(showGroupView, &ShowGroupView::changedTaskGroup, this, &TaskUI::reloadTasksWithNo);
    //自定义事件
    this->installEventFilter(this);

}

bool TaskUI::eventFilter(QObject *obj, QEvent *event)
{
    if(event->type() == QEvent::Show || event->type() == QEvent::Resize)
    {
        //挪动添加按钮
        int marginSize = 5;
        int baseAddTaskSize = 40;
        FlatUI::setNormalAddTaskBtnQss(this->addTaskBtn);
        this->addTaskBtn->setFixedSize(baseAddTaskSize, baseAddTaskSize);
        QRect taskViewRect = this->taskView->geometry();
        int taskViewX = taskViewRect.x();
        int taskViewY = taskViewRect.y();
        int taskViewWidth = taskViewRect.width();
        int taskViewHeight = taskViewRect.height();
        int addBtnX = taskViewX + taskViewWidth - baseAddTaskSize - marginSize;
        int addBtnY = taskViewY + taskViewHeight - baseAddTaskSize - marginSize;
        this->addTaskBtn->move(addBtnX, addBtnY);
        //挪动添加页面
        this->addTaskView->setFixedHeight(baseAddTaskSize);
        int addTaskWidth = taskViewRect.width() - baseAddTaskSize - marginSize * 3;
        this->addTaskView->setFixedWidth(addTaskWidth);
        int x = taskViewX + marginSize;
        int y = taskViewY + taskViewHeight - baseAddTaskSize - marginSize;
        this->addTaskView->move(x, y);
        return true;
    }
    return true;
}

void TaskUI::reloadTasksWithNo(int taskId)
{
    this->reloadTasks(this->typeId, this->groupId);
}

void TaskUI::reloadTasks(int typeId, int groupId)
{
    this->typeId = typeId;
    this->groupId = groupId;
    this->addTaskView->setTypeIdAndGroupId(typeId, groupId);
    reloadTaskList(typeId, groupId);
}

/**
 * 加载任务列表
 * @brief frmFlatUI::reloadTaskList
 * @param type
 * @param groupId
 */
void TaskUI::reloadTaskList(int type, int groupId)
{
    //清理
    this->taskTableModel->clear();
    //重新加载
    this->taskTableModel->setTable("td_task");
    QString filterSql;
    if(this->enableStatus == 2)
    {
        filterSql = QString("");
    }
    else
    {
        filterSql = QString(" enable_status=%1 ")
                .arg(enableStatus);
    }
    QString orderBySql = QString(" ORDER BY enable_status ASC,id DESC, important_status DESC, collection_status DESC");
    QString filterParamSql = "";
    //构建查询条件
    if(type == GroupUI::Group && groupId > 0)  //按照选择的分组查询
    {
        filterParamSql = QString(" group_id='%1' ")
                .arg(groupId);
    }
    else if(type == GroupUI::Today) //我的一天
    {
        filterParamSql = QString(" plan_time >= '%1' ")
                            .arg(QDate::currentDate().toString("yyyy-MM-dd"));
    }
    else if (type == GroupUI::All)  //全部任务
    {
        filterParamSql = QString(" 1=1 ");
    }
    else if(type == GroupUI::Important)  //重要任务
    {
        filterParamSql = QString(" important_status='%1' ")
                                  .arg(1);
    }
    else if(type == GroupUI::Collection)  //我的收藏
    {
        filterParamSql = QString(" collection_status='%1' ")
                                  .arg(1);
    }
    if(!filterSql.isEmpty())
    {
        filterSql.append(" AND ");
    }
    filterSql.append(filterParamSql)
             .append(orderBySql);
    this->taskTableModel->setFilter(filterSql);


    this->taskTableModel->select();
    //设置表头和行号
    QHeaderView *horizontalHeader = this->taskView->horizontalHeader();
    horizontalHeader->setVisible(false);
    this->taskView->verticalHeader()->setVisible(false);
    this->taskView->verticalHeader()->setDefaultSectionSize(42);
    this->taskView->setItemDelegate(taskItemDelegate);
    this->taskView->setModel(taskTableModel);
    //隐藏列表
    for(int i = Id; i <= CollectionStatus; i++) {
        if(TaskText != i)
        {
            this->taskView->setColumnHidden(i, true);
        }
    }
    this->taskView->horizontalHeader()->setStretchLastSection(true);
    //设置无法修改
    this->taskView->setEditTriggers(QAbstractItemView::NoEditTriggers);
}

void TaskUI::delByGroupId(int delGroupId)
{
    dbUtil::delTaskByGroupId(delGroupId);
    this->reloadTasks(GroupUI::Today, 0);
}

void TaskUI::chooseTask(const QModelIndex &index)
{
    emit itemChecked(index);
}

void TaskUI::goToTaskDetail(const QModelIndex &index)
{
    //获取外部的坐标
    QRect rect = this->parentWidget()->geometry();
    this->taskDetailUI->setWindowModality(Qt::ApplicationModal);
    this->taskDetailUI->show();
    int x = rect.x() + rect.width()/2 - this->taskDetailUI->width()/2;
    int y = rect.y() + rect.height()/2 - this->taskDetailUI->height()/2;
    this->taskDetailUI->move(x, y);
}

void TaskUI::goTaskDetail()
{
    //获取外部的坐标
    QRect rect = this->parentWidget()->geometry();
    this->taskDetailUI->setWindowModality(Qt::ApplicationModal);
    this->taskDetailUI->show();
    int x = rect.x() + rect.width()/2 - this->taskDetailUI->width()/2;
    int y = rect.y() + rect.height()/2 - this->taskDetailUI->height()/2;
    this->taskDetailUI->move(x, y);
}

void TaskUI::checkedAll()
{
    this->enableStatus = 2;
    FlatUI::setTaskStatusNormalBtn(this->inCompleteBtn);
    FlatUI::setTaskStatusNormalBtn(this->completeBtn);
    FlatUI::setTaskStatusClickBtn(this->allTaskBtn);
    this->reloadTasks(this->typeId, this->groupId);
}
void TaskUI::checkedInComplete()
{
    this->enableStatus = EnableStatus::Disable;
    FlatUI::setTaskStatusClickBtn(this->inCompleteBtn);
    FlatUI::setTaskStatusNormalBtn(this->completeBtn);
    FlatUI::setTaskStatusNormalBtn(this->allTaskBtn);
    this->reloadTasks(this->typeId, this->groupId);
}
void TaskUI::checkedComplete()
{
    this->enableStatus = EnableStatus::Enable;
    FlatUI::setTaskStatusNormalBtn(this->inCompleteBtn);
    FlatUI::setTaskStatusClickBtn(this->completeBtn);
    FlatUI::setTaskStatusNormalBtn(this->allTaskBtn);
    this->reloadTasks(this->typeId, this->groupId);
}

void TaskUI::showAddTaskUI()
{
    if(this->isAddShow)
    {
        this->addTaskView->hide();
    }
    else
    {
        this->addTaskView->show();
    }
    this->isAddShow = !this->isAddShow;
}

void TaskUI::slotContextMenu(QPoint pos) {
    QModelIndex index = this->taskView->indexAt(pos);
    if (index.isValid()) {
        QModelIndex primaryIdIndex = index.model()->index(index.row(), 0);
        int taskId = index.model()->data(primaryIdIndex)
                .toInt();
        this->doubleClickMenu->exec(QCursor::pos());
        this->rightClickTaskId = taskId;
        emit itemChecked(index);
        emit clickTask(taskId);
    }
}

void TaskUI::showChangeTaskView() {
    //获取外部的坐标
    QRect rect = this->parentWidget()->geometry();
    this->showGroupView->setWindowModality(Qt::ApplicationModal);
    this->showGroupView->show();
    int x = rect.x() + rect.width()/2 - this->showGroupView->width()/2;
    int y = rect.y() + rect.height()/2 - this->showGroupView->height()/2;
    this->showGroupView->move(x, y);
}
