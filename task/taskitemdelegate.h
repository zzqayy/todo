#ifndef TASKITEMDELEGATE_H
#define TASKITEMDELEGATE_H

#include <QModelIndex>
#include <QStyledItemDelegate>
#include <qpainter.h>
#include <qsize.h>
#include <qpainterpath.h>

class TaskItemDelegate : public QStyledItemDelegate
{
public:
    explicit TaskItemDelegate(QObject *parent = nullptr);
    //重写重画函数
    void paint(QPainter * painter,const QStyleOptionViewItem & option,const QModelIndex & index) const;
    QSize sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const;
    /**
     * check事件
     * @brief checkEvent
     * @param event
     * @param model
     * @param option
     * @param index
     * @return
     */
    bool statusCheckEvent(QEvent *event, QAbstractItemModel *model, const QStyleOptionViewItem &option, const QModelIndex &index);
};

#endif // TASKITEMDELEGATE_H
