#ifndef ADDTASKVIEW_H
#define ADDTASKVIEW_H

#include <QWidget>
#include "utils/flatui.h"
#include <qmessagebox.h>
#include <qboxlayout.h>
#include <qcalendarwidget.h>
#include <qdatetimeedit.h>
#include <qlabel.h>
#include <qlineedit.h>
#include <qpushbutton.h>
#include "tasklineedit.h"
#include "utils/dbutil.h"
#include <qabstractitemmodel.h>
#include <qapplication.h>
#include <qcheckbox.h>

class AddTaskView : public QFrame
{
    Q_OBJECT
public:
    explicit AddTaskView(QFrame *parent = 0);
    ~AddTaskView();
    void setTypeIdAndGroupId(int typeId, int groupId);

private:
    void init();
    int groupId = 0;
    int typeId = 0;
    QHBoxLayout *contextLayout = new QHBoxLayout(this);
    TaskLineEdit *taskTxtEdit = new TaskLineEdit(this);
    QDateEdit *taskDate = new QDateEdit();
    QCheckBox *importantBox = new QCheckBox("重要");

    void addTask();
signals:
    void addVal(int typeId, int groupId);
};

#endif // ADDTASKVIEW_H
