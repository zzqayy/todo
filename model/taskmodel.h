//
// Created by zzq on 2022/3/16.
//

#ifndef TODO_TASKMODEL_H
#define TODO_TASKMODEL_H

#include <QString>
#include <QDate>

class TaskModel {
private:
    int id;
    QString taskText;
    int groupId;
    QDate createTime;
    QDate planTime;
    QDate doneTime;
    int enableStatus;
    int importantStatus;
    int collectionStatus;
public:

    TaskModel(int id, const QString &taskText, int groupId, const QDate &createTime, const QDate &planTime,
              const QDate &doneTime, int enableStatus, int importantStatus, int collectionStatus) : id(id),
                                                                                                    taskText(taskText),
                                                                                                    groupId(groupId),
                                                                                                    createTime(
                                                                                                            createTime),
                                                                                                    planTime(planTime),
                                                                                                    doneTime(doneTime),
                                                                                                    enableStatus(
                                                                                                            enableStatus),
                                                                                                    importantStatus(
                                                                                                            importantStatus),
                                                                                                    collectionStatus(
                                                                                                            collectionStatus) {}
    int getId() const {
        return id;
    }

    void setId(int id) {
        TaskModel::id = id;
    }

    const QString &getTaskText() const {
        return taskText;
    }

    void setTaskText(const QString &taskText) {
        TaskModel::taskText = taskText;
    }

    int getGroupId() const {
        return groupId;
    }

    void setGroupId(int groupId) {
        TaskModel::groupId = groupId;
    }

    const QDate &getCreateTime() const {
        return createTime;
    }

    void setCreateTime(const QDate &createTime) {
        TaskModel::createTime = createTime;
    }

    const QDate &getPlanTime() const {
        return planTime;
    }

    void setPlanTime(const QDate &planTime) {
        TaskModel::planTime = planTime;
    }

    const QDate &getDoneTime() const {
        return doneTime;
    }

    void setDoneTime(const QDate &doneTime) {
        TaskModel::doneTime = doneTime;
    }

    int getEnableStatus() const {
        return enableStatus;
    }

    void setEnableStatus(int enableStatus) {
        TaskModel::enableStatus = enableStatus;
    }

    int getImportantStatus() const {
        return importantStatus;
    }

    void setImportantStatus(int importantStatus) {
        TaskModel::importantStatus = importantStatus;
    }

    int getCollectionStatus() const {
        return collectionStatus;
    }

    void setCollectionStatus(int collectionStatus) {
        TaskModel::collectionStatus = collectionStatus;
    }
};

class ExportTaskModel {
private:
    QString taskTxt;
    QString doneTime;
public:
    ExportTaskModel(const QString &taskTxt, const QString &doneTime) : taskTxt(taskTxt), doneTime(doneTime) {}

    const QString &getTaskTxt() const {
        return taskTxt;
    }

    void setTaskTxt(const QString &taskTxt) {
        ExportTaskModel::taskTxt = taskTxt;
    }

    const QString &getDoneTime() const {
        return doneTime;
    }

    void setDoneTime(const QString &doneTime) {
        ExportTaskModel::doneTime = doneTime;
    }
};
#endif //TODO_TASKMODEL_H
