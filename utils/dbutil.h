#ifndef DBUTIL_H
#define DBUTIL_H

#include <QObject>
#include <QSqlDatabase>
#include <QDebug>
#include <QMessageBox>
#include <QSqlError>
#include <QSqlQuery>
#include <QDir>
#include <QStandardPaths>
#include <QStringList>
#include "ConfigUtil.h"
#include "model/taskmodel.h"

class dbUtil : public QObject
{
    Q_OBJECT
public:
    explicit dbUtil(QObject *parent = nullptr);
    ~ dbUtil();
    static void initSql();
    const static QString dbPath;
    const static QString groupListSql;
    static void addGroup(QString groupName);
    /**
     * 创建链接
     * @brief getDbConnect
     * @return
     */
    static QSqlDatabase getDbConnect(QString typeName = "defalut_connect");
    /**
     * 销毁链接
     * @brief distoryDB
     */
    static void distoryDB(QSqlDatabase db);
    /**
     * 删除主键
     * @brief delGroupByPrimaryId
     * @param primaryId
     */
    static void delGroupByPrimaryId(int primaryId);

    static void delTaskByGroupId(int groupId);

    static bool addTask(QString taskContext,
                        int groupId,
                        QDate taskDate,
                        int importantStatus);
    static bool updateTask(int taskId
                           , QString taskTxt
                           , QDate planTime
                           , QDateTime doneTime
                           , int enableStatus
                           , int importantStatus
                           , int collectionStatus
                           );
    static bool delTaskByPrimaryId(int primaryId);
    static bool changeTaskGroup(int primaryId, int groupId);
    static QList<ExportTaskModel> taskList(int id);
private:
signals:


};

#endif // DBUTIL_H
