//
// Created by zzq on 2022/2/10.
//
#include "ConfigUtil.h"

const QString ConfigUtil::appHome = QStandardPaths::writableLocation(QStandardPaths::HomeLocation).append("/.todo");
const QString ConfigUtil::configPath = ConfigUtil::appHome + "/config.ini";
static QSettings* globalSetting;
static QString defaultStyle;

ConfigUtil::ConfigUtil(QObject *parent) : QObject(parent) {

}

ConfigUtil::~ConfigUtil() {

}

void ConfigUtil::initConfig() {
    //加载配置文件目录
    QDir dir;
    if(!dir.exists(ConfigUtil::appHome) && !dir.mkpath(ConfigUtil::appHome)) {
        QMessageBox::warning(NULL, "消息提示", "创建文件夹失败");
    };
    //加载配置文件
    bool hasConfigStatus = QFile::exists(configPath);
    globalSetting = new QSettings(configPath, QSettings::IniFormat);
    if (!hasConfigStatus) {
        globalSetting->setValue("/Fonts/family", "Source Serif 4");

        globalSetting->setValue("/Themes/style", "white");
        //黑色背景
        globalSetting->setValue("/black/todoUiBackgroundColor", "#2B2B2B");
        globalSetting->setValue("/black/groupSelectedPen", "#2D3E50");
        globalSetting->setValue("/black/groupSelectedBrush", "#2D3E50");
        globalSetting->setValue("/black/groupSelectedNamePen", "#A9B7C6");
        globalSetting->setValue("/black/groupUnSelectedNamePen", "#A9B7C6");
        globalSetting->setValue("/black/btnClickedColor", "#0D293E");
        globalSetting->setValue("/black/btnNormalColor", "#0D293E");
        globalSetting->setValue("/black/btnHoverColor", "#0D293E");
        //白色背景
        globalSetting->setValue("/white/todoUiBackgroundColor", "#F2F2F2");
        globalSetting->setValue("/white/groupSelectedPen", "#2D3E50");
        globalSetting->setValue("/white/groupSelectedBrush", "#34495E");
        globalSetting->setValue("/white/groupSelectedNamePen", "#FFFFFF");
        globalSetting->setValue("/white/groupUnSelectedNamePen", "#2D3E50");
        //常规颜色
        globalSetting->setValue("/white/normalColor", "#3498DB");
        globalSetting->setValue("/white/normalTextColor", "#F0F0F0");
        globalSetting->setValue("/white/normalHoverColor", "#5DACE4");
        globalSetting->setValue("/white/normalHoverTextColor", "#E5FEFF");
        globalSetting->setValue("/white/normalPressColor", "#2483C7");
        globalSetting->setValue("/white/normalPressTextColor", "#A0DAFB");
        //选择颜色
        globalSetting->setValue("/white/chooseColor", "#34495E");
        globalSetting->setValue("/white/chooseTextColor", "#FFFFFF");
        globalSetting->setValue("/white/chooseHoverColor", "#4E6D8C");
        globalSetting->setValue("/white/chooseHoverTextColor", "#F0F0F0");
        globalSetting->setValue("/white/choosePressColor", "#2D3E50");
        globalSetting->setValue("/white/choosePressTextColor", "#B8C6D1");
        //删除颜色
        globalSetting->setValue("/white/delColor", "#909399");
        globalSetting->setValue("/white/delTextColor", "#FFFFFF");
        globalSetting->setValue("/white/delHoverColor", "#A2A2A2");
        globalSetting->setValue("/white/delHoverTextColor", "#FFFFFF");
        globalSetting->setValue("/white/delPressColor", "#909399");
        globalSetting->setValue("/white/delPressTextColor", "#FFFFFF");
        //警告颜色
        globalSetting->setValue("/white/dangerColor", "#E74C3C");
        globalSetting->setValue("/white/dangerTextColor", "#F0F0F0");
        globalSetting->setValue("/white/dangerHoverColor", "#EC7064");
        globalSetting->setValue("/white/dangerHoverTextColor", "#FFF5E7");
        globalSetting->setValue("/white/dangerPressColor", "#DC2D1A");
        globalSetting->setValue("/white/dangerPressTextColor", "#F5A996");
        //任务列表颜色
        globalSetting->setValue("/white/taskItemClickPenColor", "#D3D3D3");
        globalSetting->setValue("/white/taskItemClickBrushColor", "#D3D3D3");
        globalSetting->setValue("/white/taskItemPenColor", "#FFFFFF");
        globalSetting->setValue("/white/taskItemCircleClickBrushColor", "#1ABC9C");
        globalSetting->setValue("/white/taskItemCircleBrushColor", "#00000");
        globalSetting->setValue("/white/taskItemNameClickColor", "#969696");
        globalSetting->setValue("/white/taskItemNameColor", "#000000");
        globalSetting->setValue("/white/taskItemTimeColor", "#969696");
        //编辑框颜色
        globalSetting->setValue("/white/lineEditNormalColor", "#DCE4EC");
        globalSetting->setValue("/white/lineEditFocusColor", "#34495E");
        globalSetting->setValue("/white/lineEditBackgroundColor", "#FFFFFF");
        globalSetting->setValue("/white/lineEditTxtColor", "#D3D3D3");
    }
    defaultStyle = globalSetting->value("/Themes/style").toString();
}

QString ConfigUtil::getStyleValue(QString styleStr) {
    QString settingKeyStr = QString("/").append(defaultStyle)
            .append("/")
            .append(styleStr);
    return globalSetting->value(settingKeyStr).toString();
}

QString ConfigUtil::getDefaultFont() {
    return globalSetting->value("/Fonts/family").toString();
}