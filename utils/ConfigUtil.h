//
// Created by zzq on 2022/2/10.
//

#ifndef TODO_CONFIGUTIL_H
#define TODO_CONFIGUTIL_H


#include <QObject>
#include <QString>
#include <QSettings>
#include <QCoreApplication>
#include <QFile>
#include <QStandardPaths>
#include <QDir>
#include <QMessageBox>

class ConfigUtil : public QObject
{

    Q_OBJECT
public:
    explicit ConfigUtil(QObject *parent = nullptr);
    ~ ConfigUtil();
    static void initConfig();
    static QString getStyleValue(QString styleStr);
    static QString getDefaultFont();
    const static QString appHome;
private:
    const static QString configPath;
};


#endif //TODO_CONFIGUTIL_H
