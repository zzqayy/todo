﻿#pragma execution_character_set("utf-8")
#include "todoui.h"

TodoUI::TodoUI(QFrame *parent) : QFrame(parent)
{
    this->initForm();
}

TodoUI::~TodoUI()
{
}

void TodoUI::initForm()
{
    QString backgroundStyle = QString("QFrame{background:%1;}")
            .arg(ConfigUtil::getStyleValue("todoUiBackgroundColor"));
    qDebug() << "[debug] todoUiBackgroundColor is : " << backgroundStyle;
    this->setStyleSheet(backgroundStyle);

    //设置布局
    mainLayout->addWidget(this->groupUI, 3);
    mainLayout->addWidget(this->taskUI, 7);

    //状态栏
    todoBar = new TodoBar();
    connect(todoBar, &TodoBar::showMainSignal, this, &TodoUI::show);

    //初始化绘图
    connect(this->groupUI, &GroupUI::typeAndGroupChange, this->taskUI, &TaskUI::reloadTasks);
    connect(this->groupUI, &GroupUI::delTaskByGroupId, this->taskUI, &TaskUI::delByGroupId);
}

void TodoUI::closeEvent(QCloseEvent *event) {
    event->ignore();
    hide();
}

void TodoUI::setTrayIcon(const QIcon &icon)
{
    todoBar->setIcon(icon);
}

void TodoUI::showSystemTrayIcon()
{
    todoBar->show();
}

