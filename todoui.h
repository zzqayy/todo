#ifndef TODOUI_H
#define TODOUI_H

#include "task/taskdetailui.h"
#include "task/taskui.h"
#include <QStackedLayout>
#include "toolbar/todobar.h"
#include <QMenu>
#include <QApplication>
#include <QSettings>
#include "utils/ConfigUtil.h"
#include <QDebug>

class TodoUI : public QFrame
{
    Q_OBJECT

public:
    explicit TodoUI(QFrame *parent = 0);
    ~TodoUI();
    void setTrayIcon(const QIcon &icon);
    void showSystemTrayIcon();
private:
    QHBoxLayout *mainLayout = new QHBoxLayout(this);
    GroupUI *groupUI = new GroupUI(this);
    TaskUI *taskUI = new TaskUI(this);
    TodoBar *todoBar;
protected:
    virtual void closeEvent(QCloseEvent *event);
private slots:

    void initForm();

};

#endif // TODOUI_H
