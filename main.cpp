﻿#pragma execution_character_set("utf-8")

#include <qfont.h>
#include "todoui.h"
#include <QApplication>
#include <qfontdatabase.h>
#include <qicon.h>
#include "utils/ConfigUtil.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    int fontId = QFontDatabase::addApplicationFont(":/fonts/SourceSerif4-Regular.ttf");
    QString sourceSerif4 = QFontDatabase::applicationFontFamilies(fontId ).at(0);
    app.setFont(QFont(sourceSerif4, 12));

    QIcon mainIcon = QIcon(":/ico/icon.png");
    app.setWindowIcon(mainIcon);
    //初始化配置
    ConfigUtil::initConfig();
    //初始化数据库
    dbUtil::initSql();
    //开始绘图
    TodoUI todoUi;
    todoUi.setWindowTitle("待办事项");
    todoUi.setMinimumWidth(800);
    todoUi.setMinimumHeight(600);
    todoUi.show();

    //设置icon
    todoUi.setTrayIcon(mainIcon);
    todoUi.showSystemTrayIcon();

    return app.exec();
}