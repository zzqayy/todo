//
// Created by zzq on 2022/2/8.
//
#include "todobar.h"

TodoBar::TodoBar(QObject *parent) : QSystemTrayIcon(parent)
{
    trayMenu = new QMenu;
    showMainAction = new QAction(tr("显示主窗口"), trayMenu);
    exitQuitAction = new QAction(tr("退出"), trayMenu);
    trayMenu->addAction(showMainAction);
    trayMenu->addAction(exitQuitAction);
    this->setContextMenu(trayMenu);
    this->setToolTip(tr("代办事项"));

    connect(this, &TodoBar::activated, this, &TodoBar::customActivated);
    connect(showMainAction, &QAction::triggered, this, &TodoBar::showMain);
    connect(exitQuitAction, &QAction::triggered, qApp, &QApplication::quit);
}

TodoBar::~TodoBar()
{
    delete this;
}

void TodoBar::showMain()
{
    emit showMainSignal();
}

void TodoBar::customActivated(QSystemTrayIcon::ActivationReason reason)
{
    if (QSystemTrayIcon::DoubleClick == reason)
    {
        emit showMainSignal();
    }
}
