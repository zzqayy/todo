//
// Created by zzq on 2022/2/8.
//

#ifndef TODO_TODOBAR_H
#define TODO_TODOBAR_H

#include <QObject>
#include <QSystemTrayIcon>
#include <QMenu>
#include <QAction>
#include <QApplication>

class TodoBar : public QSystemTrayIcon
{
    Q_OBJECT
public:
    explicit TodoBar(QObject *parent = nullptr);
    ~TodoBar();

    signals:
        void showMainSignal();

private:
    QMenu *trayMenu;
    QAction *exitQuitAction;
    QAction *showMainAction;
private slots:
     void showMain();
     void customActivated(QSystemTrayIcon::ActivationReason reason);
};

#endif //TODO_TODOBAR_H
