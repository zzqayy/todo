#ifndef GROUPUIH_H
#define GROUPUIH_H

#include <QTableView>
#include <qpushbutton.h>
#include "utils/flatui.h"
#include <qboxlayout.h>
#include "utils/dbutil.h"
#include <qsqltablemodel.h>
#include <QHeaderView>
#include <QModelIndex>
#include <QMenu>
#include <QFileDialog>
#include "groupitemdelegate.h"
#include <QDebug>
#include "model/taskmodel.h"
#include <iostream>

using namespace std;

class GroupUI : public QFrame
{
    Q_OBJECT
public:
    explicit GroupUI(QFrame *parent = 0);
    ~GroupUI();
    enum GroupColum: int {
        Id = 0,
        GroupName = 1,
        CreateTime = 2,
        Sort = 3
    };
    enum TaskType: int {
        Today = 1,
        All = 2,
        Important = 3,
        Collection =4,
        Group = 5
    };

signals:

    void chooseGroupId(int groupId);

    void typeAndGroupChange(int typeId, int groupId);

    void delTaskByGroupId(int delGroupId);

private slots:

    void clickGroup(const QModelIndex &index);

    void todayTaskLoad();

    void allTaskLoad();

    void importantTaskLoad();

    void favorTaskLoad();

    void changeColor(int typeId);

    void slotContextMenu(QPoint pos);

    void changeGroupId(int groupId);

    void exportGroupTask();

private:
    QPushButton *allTaskBtn = new QPushButton("全部任务");
    QPushButton *favorTaskBtn = new QPushButton("我的收藏");
    QPushButton *importantTaskBtn = new QPushButton("重要任务");
    QPushButton *todayTaskBtn = new QPushButton("今日任务");
    QTableView *groupTable = new QTableView(this);
    QPushButton *addGroupBtn = new QPushButton("添 加");
    QPushButton *delGroupBtn = new QPushButton("删 除");
    QSqlTableModel *groupTableModel;
    GroupItemDelegate *groupItemDelegate;
    QVBoxLayout *mainLayout = new QVBoxLayout(this);
    QHBoxLayout *btnLayout = new QHBoxLayout();
    int choseGroupId;

    QMenu *rightMenu = new QMenu(this);
    QAction *exportAction = new QAction(tr("导出"));

    void init();
    void reloadGroup();
    void addGroupList();
    void delGroupList();
};

#endif // GROUPUIH_H
