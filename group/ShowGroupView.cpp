//
// Created by zzq on 2022/3/9.
//

#include "ShowGroupView.h"

ShowGroupView::ShowGroupView(QFrame *parent): QFrame(parent) {
    groupTableView = new QTableView();
    this->mainLayout->addWidget(groupTableView);
    groupItemDelegate = new GroupItemDelegate(this->groupTableView);
    //初始化数据
    groupTableModel = new QSqlTableModel(this->groupTableView, dbUtil::getDbConnect("group_show_connect"));
    this->groupTableModel->clear();
    //查询任务版块并渲染任务版块
    this->groupTableModel->setTable("td_group");
    this->groupTableModel->setSort(3, Qt::SortOrder::DescendingOrder);
    this->groupTableModel->setSort(0, Qt::SortOrder::DescendingOrder);
    this->groupTableModel->select();
    //去除表头和行号
    this->groupTableView->horizontalHeader()->setVisible(false);
    this->groupTableView->verticalHeader()->setVisible(false);
    this->groupTableView->setItemDelegate(groupItemDelegate);
    this->groupTableView->setModel(groupTableModel);
    //隐藏列表
    this->groupTableView->setColumnHidden(GroupColum::Id, true);
    this->groupTableView->setColumnHidden(GroupColum::CreateTime, true);
    this->groupTableView->setColumnHidden(GroupColum::Sort, true);
    //设置显示的这一列扩展到最大
    this->groupTableView->horizontalHeader()->setStretchLastSection(true);
    this->groupTableView->setEditTriggers(QAbstractItemView::NoEditTriggers);
    connect(this->groupTableView, &QTableView::clicked, this, &ShowGroupView::changeTaskGroup);
}

ShowGroupView::~ShowGroupView() {

}

void ShowGroupView::setChooseTaskId(int chooseTaskId) {
    this->chooseTaskId = chooseTaskId;
}

void ShowGroupView::changeTaskGroup(const QModelIndex &index) {
    if (index.isValid()) {
        QModelIndex primaryIdIndex = index.model()->index(index.row(), 0);
        int groupId = index.model()->data(primaryIdIndex)
                .toInt();
        dbUtil::changeTaskGroup(this->chooseTaskId, groupId);
        emit changedTaskGroup(chooseTaskId);
        this->close();
    }
}
