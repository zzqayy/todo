#include "groupui.h"

#include <qmessagebox.h>

GroupUI::GroupUI(QFrame *parent): QFrame(parent)
{
    //初始化变量
    init();
}

GroupUI::~GroupUI()
{

}

void GroupUI::init()
{
    //设置初始化样式
    FlatUI::setNormalBtnColor(this->addGroupBtn);
    FlatUI::setDangerButtonQss(this->delGroupBtn);
    //我的一天
    FlatUI::setChooseBtnColor(this->todayTaskBtn);
    //全部任务
    FlatUI::setNormalBtnColor(this->allTaskBtn);
    //我的收藏
    FlatUI::setNormalBtnColor(this->favorTaskBtn);
    //重要任务
    FlatUI::setNormalBtnColor(this->importantTaskBtn);
    this->groupTable->setStyleSheet("QTableView {border: 1px solid gray;border-radius:3px}");
    this->groupTable->setContextMenuPolicy(Qt::CustomContextMenu);

    //设置布局
    mainLayout->addWidget(this->todayTaskBtn, 1);
    mainLayout->addWidget(this->allTaskBtn, 1);
//    mainLayout->addWidget(this->importantTaskBtn, 1);
//    mainLayout->addWidget(this->favorTaskBtn, 1);
    mainLayout->addWidget(this->groupTable, 11);
    btnLayout->addWidget(this->addGroupBtn, 1);
    btnLayout->addWidget(this->delGroupBtn, 1);
    mainLayout->addLayout(btnLayout, 1);
    groupItemDelegate = new GroupItemDelegate(this->groupTable);
    //初始化数据
    groupTableModel = new QSqlTableModel(this->groupTable, dbUtil::getDbConnect("group_connect"));
    //刷新事件
    reloadGroup();
    //初始化任务
    this->changeColor(GroupUI::Today);
    //颜色变化
    connect(this, &GroupUI::typeAndGroupChange, this, &GroupUI::changeColor);

    //我的一天
    connect(this->todayTaskBtn, &QPushButton::clicked, this, &GroupUI::todayTaskLoad);
    //所有任务
    connect(this->allTaskBtn, &QPushButton::clicked, this, &GroupUI::allTaskLoad);
    //我的收藏
    connect(this->favorTaskBtn, &QPushButton::clicked, this, &GroupUI::favorTaskLoad);
    //重要任务
    connect(this->importantTaskBtn, &QPushButton::clicked, this, &GroupUI::importantTaskLoad);

    //添加分组事件
    connect(this->addGroupBtn, &QPushButton::clicked, this, &GroupUI::addGroupList);
    //删除分组时间
    connect(this->delGroupBtn, &QPushButton::clicked, this, &GroupUI::delGroupList);
    //监听点击事件
    connect(this->groupTable, &QTableView::clicked, this, &GroupUI::clickGroup);

    //右键监听
    this->exportAction->setParent(this->rightMenu);
    this->rightMenu->addAction(this->exportAction);

    connect(this->groupTable, &QTableView::customContextMenuRequested, this, &GroupUI::slotContextMenu);
    connect(this, &GroupUI::chooseGroupId, this, &GroupUI::changeGroupId);
    //监听导出事件
    connect(this->exportAction, &QAction::triggered, this, &GroupUI::exportGroupTask);
}

void GroupUI::clickGroup(const QModelIndex &index)
{
    if(index.row() != -1) {
        QModelIndex primaryIdIndex = index.model()->index(index.row(), 0);
        choseGroupId = index.model()->data(primaryIdIndex)
                .toInt();
        if(choseGroupId > 0)
        {
            emit typeAndGroupChange(GroupUI::Group, choseGroupId);
        }
    }
}

void GroupUI::reloadGroup() {
    this->groupTableModel->clear();
    //查询任务版块并渲染任务版块
    this->groupTableModel->setTable("td_group");
    this->groupTableModel->setSort(3, Qt::SortOrder::DescendingOrder);
    this->groupTableModel->setSort(0, Qt::SortOrder::DescendingOrder);
    this->groupTableModel->select();
    //去除表头和行号
    this->groupTable->horizontalHeader()->setVisible(false);
    this->groupTable->verticalHeader()->setVisible(false);
    this->groupTable->setItemDelegate(groupItemDelegate);
    this->groupTable->setModel(groupTableModel);
    //隐藏列表
    this->groupTable->setColumnHidden(GroupColum::Id, true);
    this->groupTable->setColumnHidden(GroupColum::CreateTime, true);
    this->groupTable->setColumnHidden(GroupColum::Sort, true);
    //设置显示的这一列扩展到最大
    this->groupTable->horizontalHeader()->setStretchLastSection(true);
}

void GroupUI::addGroupList() {
    groupTableModel->insertRow(0);
    groupTableModel->setData(groupTableModel->index(0, GroupColum::GroupName), "新建任务");
    groupTableModel->setData(groupTableModel->index(0, GroupColum::CreateTime), QDateTime::currentDateTime());
    groupTableModel->setData(groupTableModel->index(0, GroupColum::Sort), 1);
    if(!groupTableModel->submitAll()) {
        QMessageBox::warning(this, "提示", "添加失败");
    }
}

void GroupUI::delGroupList() {
    int delStatus = QMessageBox::question(this,
                          tr("删除分组"),
                          QString("确认删除分组?"), QMessageBox::Yes | QMessageBox::No
                          );
    if(delStatus == QMessageBox::Yes) {
        if(this->groupTable->currentIndex().row() != -1) {
            //获取选中列的列表QVariant localData =
            QModelIndex clickedIndex = this->groupTable->currentIndex();
            int row = clickedIndex.row();
            int delGroupId = clickedIndex.model()->index(row, 0)
                    .data()
                    .toInt();
            groupTableModel->removeRow(row);
            if(!groupTableModel->submitAll()) {
                QMessageBox::warning(this, "提示", "删除失败");
            }else {
                groupTableModel->clear();
                reloadGroup();
                emit delTaskByGroupId(delGroupId);
            }

        }else {
            QMessageBox::warning(this, "提示", "必须选中才可删除相应的分组");
        }
    }
}

void GroupUI::todayTaskLoad()
{
    emit GroupUI::typeAndGroupChange(GroupUI::Today, 0);
}

void GroupUI::allTaskLoad()
{
    emit GroupUI::typeAndGroupChange(GroupUI::All, 0);
}

void GroupUI::importantTaskLoad()
{
    emit GroupUI::typeAndGroupChange(GroupUI::Important, 0);
}

void GroupUI::favorTaskLoad()
{
    emit GroupUI::typeAndGroupChange(GroupUI::Collection, 0);
}

void GroupUI::changeColor(int typeId)
{
    if(typeId == GroupUI::Today) {
        FlatUI::setChooseBtnColor(this->todayTaskBtn);
        FlatUI::setNormalBtnColor(this->allTaskBtn);
        FlatUI::setNormalBtnColor(this->favorTaskBtn);
        FlatUI::setNormalBtnColor(this->importantTaskBtn);
        this->groupTable->setCurrentIndex(QModelIndex());
    }else if(typeId == GroupUI::All) {
        FlatUI::setChooseBtnColor(this->allTaskBtn);
        FlatUI::setNormalBtnColor(this->todayTaskBtn);
        FlatUI::setNormalBtnColor(this->favorTaskBtn);
        FlatUI::setNormalBtnColor(this->importantTaskBtn);
        this->groupTable->setCurrentIndex(QModelIndex());
    }else if(typeId == GroupUI::Important) {
        FlatUI::setChooseBtnColor(this->importantTaskBtn);
        FlatUI::setNormalBtnColor(this->todayTaskBtn);
        FlatUI::setNormalBtnColor(this->allTaskBtn);
        FlatUI::setNormalBtnColor(this->favorTaskBtn);
        this->groupTable->setCurrentIndex(QModelIndex());
    }else if(typeId == GroupUI::Collection) {
        FlatUI::setChooseBtnColor(this->favorTaskBtn);
        FlatUI::setNormalBtnColor(this->todayTaskBtn);
        FlatUI::setNormalBtnColor(this->allTaskBtn);
        FlatUI::setNormalBtnColor(this->importantTaskBtn);
        this->groupTable->setCurrentIndex(QModelIndex());
    }else if(typeId == GroupUI::Group) {
        FlatUI::setNormalBtnColor(this->favorTaskBtn);
        FlatUI::setNormalBtnColor(this->todayTaskBtn);
        FlatUI::setNormalBtnColor(this->allTaskBtn);
        FlatUI::setNormalBtnColor(this->importantTaskBtn);
    }
}

void GroupUI::slotContextMenu(const QPoint pos) {
    QModelIndex index = this->groupTable->indexAt(pos);
    if (index.isValid()) {
        QModelIndex primaryIdIndex = index.model()->index(index.row(), 0);
        int groupId = index.model()->data(primaryIdIndex)
                .toInt();
        this->rightMenu->exec(QCursor::pos());
        emit chooseGroupId(groupId);
    }
}

void GroupUI::changeGroupId(int groupId) {
    this->choseGroupId = groupId;
}

void GroupUI::exportGroupTask() {
    QMessageBox::about(nullptr, "提示", "敬请期待");
//    QString filePath = QFileDialog::getSaveFileName(this, tr("保存到"),
//                                                    QString(), tr("EXCEL files (*.xls *.xlsx)"));
//    QList<ExportTaskModel> taskModelList  = dbUtil::taskList(this->choseGroupId);
//    for (const auto &item : taskModelList) {
//        qDebug() << item.getTaskTxt() << item.getDoneTime();
//    }
}
