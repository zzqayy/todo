#ifndef GROUPITEMDELEGATE_H
#define GROUPITEMDELEGATE_H

#include <QPainterPath>
#include <QStyledItemDelegate>
#include <QPainter>
#include <QModelIndex>
#include "utils/ConfigUtil.h"

class GroupItemDelegate : public QStyledItemDelegate
{
    Q_OBJECT
public:
    GroupItemDelegate(QObject *parent = 0);
    ~GroupItemDelegate();
    //重写重画函数
    void paint(QPainter * painter,const QStyleOptionViewItem & option,const QModelIndex & index) const;
    QSize sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const;
};

#endif // GROUPITEMDELEGATE_H
