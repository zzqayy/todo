//
// Created by zzq on 2022/3/9.
//

#ifndef TODO_SHOWGROUPVIEW_H
#define TODO_SHOWGROUPVIEW_H


#include <QFrame>
#include <QSqlTableModel>
#include <QTableView>
#include <QHBoxLayout>
#include "QDebug"
#include "groupitemdelegate.h"
#include "utils/dbutil.h"
#include "qheaderview.h"
#include <QAbstractItemView>

class ShowGroupView : public QFrame
{
    Q_OBJECT
public:
    enum GroupColum: int {
        Id = 0,
        GroupName = 1,
        CreateTime = 2,
        Sort = 3
    };

    ShowGroupView(QFrame *parent = 0);
    ~ShowGroupView();

    QHBoxLayout *mainLayout = new QHBoxLayout(this);
    QTableView *groupTableView;
    QSqlTableModel *groupTableModel;
    GroupItemDelegate *groupItemDelegate;
signals:
    void changedTaskGroup(int taskId);
private:
    int chooseTaskId = 0;

public slots:
    void setChooseTaskId(int chooseTaskId);
    void changeTaskGroup(const QModelIndex &index);

};


#endif //TODO_SHOWGROUPVIEW_H
