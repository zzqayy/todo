#include "groupitemdelegate.h"



GroupItemDelegate::GroupItemDelegate(QObject *parent) : QStyledItemDelegate{parent}
{

}

GroupItemDelegate::~GroupItemDelegate()
{

}

void GroupItemDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    if(index.isValid())
    {
        QVariant variant = index.data(Qt::DisplayRole);
        if(!variant.isValid()) {
            return;
        }
        painter->save();
        //用来在视图中画一个item
        QStyleOptionViewItem viewOption(option);

        QRectF rect;
        rect.setX(option.rect.x());
        rect.setY(option.rect.y());
        rect.setWidth(option.rect.width()-1);
        rect.setHeight(option.rect.height()-1);

        //QPainterPath画圆角矩形
        const qreal radius = 3;
        QPainterPath path;
        path.moveTo(rect.topRight() - QPointF(radius, 0));
        path.lineTo(rect.topLeft() + QPointF(radius, 0));
        path.quadTo(rect.topLeft(), rect.topLeft() + QPointF(0, radius));
        path.lineTo(rect.bottomLeft() + QPointF(0, -radius));
        path.quadTo(rect.bottomLeft(), rect.bottomLeft() + QPointF(radius, 0));
        path.lineTo(rect.bottomRight() - QPointF(radius, 0));
        path.quadTo(rect.bottomRight(), rect.bottomRight() + QPointF(0, -radius));
        path.lineTo(rect.topRight() + QPointF(0, radius));
        path.quadTo(rect.topRight(), rect.topRight() + QPointF(-radius, -0));

        //绘制数据位置
        QRect NameRect = QRect(20, rect.top(), rect.width()-30, 30);

        if(option.state.testFlag(QStyle::State_Selected))
        {
            painter->setPen(QPen(QColor(ConfigUtil::getStyleValue("groupSelectedPen"))));
            painter->setBrush(QColor(ConfigUtil::getStyleValue("groupSelectedBrush")));
            painter->drawPath(path);
            //绘制名字
            painter->setPen(QPen(ConfigUtil::getStyleValue("groupSelectedNamePen")));
            painter->drawText(NameRect,Qt::AlignVCenter, variant.toString());
        }
        else{
            painter->setPen(QPen(QColor(ConfigUtil::getStyleValue("groupSelectedNamePen"))));
            painter->setBrush(Qt::NoBrush);
            painter->drawPath(path);
            //绘制名字
            painter->setPen(QPen(ConfigUtil::getStyleValue("groupUnSelectedNamePen")));
            painter->drawText(NameRect,Qt::AlignVCenter, variant.toString());
        }

    }
}

QSize GroupItemDelegate::sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    return QSize(160, 30);
}
